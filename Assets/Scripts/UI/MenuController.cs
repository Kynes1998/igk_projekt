using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuController : MonoBehaviour
{
    public List<GameObject> panels;
    public InputField StartName;
    public InputField LoadName;

    public void StartNewGame()
    {
        SaveManager.instance.CreateNewSaveFile(StartName.text);
    }

    public void LoadGame()
    {
        SaveManager.instance.LoadGame(LoadName.text);
    }

    public void OpenPanel(GameObject panel)
    {
        foreach (var item in panels)
        {
            item.SetActive(false);

            if(item == panel)
            {
                item.SetActive(true);
            }
        }
    }

    public void ExitGame()
    {
        Application.Quit();
    }
}
