using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class Slots : MonoBehaviour
{
    public Item item;

    public Image icon;

    public bool isEquipmentSlot = false;

    public bool isInventorySlot = false;

    public bool isChestSlot = false;

    public bool isItemEquipped = false;

    public bool isFirstSlot = false;

    public bool isShieldEquipped = false;

    public Button button;

    public DropItemClick dropItemClick;

    public GameObject leftHandSlot;

    Equipment equipment;

    PlayerController player;

    Animator playerAnim;

    public delegate void OnFirstSlotChanged();
    public static event OnFirstSlotChanged onFirstSlotChanged;

    private void Start()
    {
        equipment = Equipment.instance;
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
        playerAnim = player.GetComponent<Animator>();
        isItemEquipped = false;
        leftHandSlot = player.leftHandSlot;

        if (!isEquipmentSlot)
        {
            dropItemClick.dropClick += DropItem;
        }
        else if(isEquipmentSlot)
        {
            dropItemClick.shieldAction += EquipShield;
        }
    }

    public void SetSlot(Item newItem)
    {
        item = newItem;

        icon.sprite = item.icon;
        icon.color = item.color;
        icon.enabled = true;
    }

    public void ClearSlot()
    {
        item = null;
        icon.sprite = null;
        icon.enabled = false;
        icon.color = Color.white;
    }

    public void ClickOnButton()
    {
        if(isInventorySlot && !GameController.instance.isChestUiVisible)
        {
            if (item != null)
                item.AddItemToEq();

            if(onFirstSlotChanged != null)
                onFirstSlotChanged.Invoke();
        }
        else if(isInventorySlot && GameController.instance.isChestUiVisible)
        {
            if (item != null)
                item.AddItemToChest();
        }
        else if(isChestSlot && GameController.instance.isChestUiVisible)
        {
            if (item != null)
                item.AddChestItemToInv();
        }
        else if(isEquipmentSlot)
        {
            if (item != null)
            {
                if (item.itemType != ItemType.Shield)
                {
                    UnEquip();
                    item.AddItemToInv();
                }
                else if (item.itemType == ItemType.Shield && !isShieldEquipped)
                {
                    item.AddItemToInv();
                }
                else if (item.itemType == ItemType.Shield && isShieldEquipped)
                {
                    return;
                }
            }
        }

        /*if(isInventorySlot && GameController.instance.isChestUiVisible && item != null)
        {
            item.AddItemToChest();
        }*/
    }
    public void Equip()
    {
        if(isEquipmentSlot && item != null)
        {
            item.EquipItem();
            isItemEquipped = true;
        }
    }

    public void UnEquip()
    {
        if (isEquipmentSlot && isItemEquipped && item.itemType != ItemType.Shield)
        {
            equipment.UnEquip();
            isItemEquipped = false;
        }
    }

    public IEnumerator Consume()
    {
        if(isEquipmentSlot && item != null && item.Eatable)
        {
            yield return new WaitForSeconds(4f);
            GetFoodStats(item);
            equipment.Remove(item);
            
            equipment.rightHandSlot.gameObject.SetActive(false);
        }
        
        player.isEating = false;
        player.isDrinking = false;
        playerAnim.SetBool("isEating", player.isEating);
        playerAnim.SetBool("isDrinking", player.isDrinking);
        yield return null;
    }

    public void GetFoodStats(Item item)
    {
        item.GetFoodStats();
    }

    public void DropItem()
    {
        if (item != null && isInventorySlot)
        {
            item.DropItem();
        }
    }

    public void EquipShield()
    {
        if(isEquipmentSlot && item != null && !isShieldEquipped)
        {
            leftHandSlot.gameObject.SetActive(true);
            item.EquipShield();
            player.stats.Defence += item.armorModifier;
            dropItemClick.shieldAction -= EquipShield;
            dropItemClick.shieldAction += UnEquipShield;
            isShieldEquipped = true;
        }
    }

    public void UnEquipShield()
    {
        if(isEquipmentSlot && item != null && isShieldEquipped)
        {
            leftHandSlot.gameObject.SetActive(false);
            equipment.UnEquipShield();
            player.stats.Defence -= item.armorModifier;
            isShieldEquipped = false;
            dropItemClick.shieldAction -= UnEquipShield;
            dropItemClick.shieldAction += EquipShield;
        }
    }


}
