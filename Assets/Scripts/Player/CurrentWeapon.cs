using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CurrentWeapon : MonoBehaviour
{
    public static CurrentWeapon instance;
    public Item item;
    PlayerController player;
    public BoxCollider[] weaponColliders;
    public BoxCollider boxCollider;


    private void Awake()
    {
        if (instance != null)
        {
            return;
        }
        instance = this;
    }

    private void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
        boxCollider = GetComponent<BoxCollider>();
        disableWeapon();
    }

    private void Update()
    {
        GetBoxCollider(item);
    }

    private void OnEnable()
    {
        Equipment.instance.onStatsChanged -= DeductWeaponAttack;
        Equipment.instance.onStatsChanged += GetWeaponAttack;
    }
    private void OnDisable()
    {
        Equipment.instance.onStatsChanged -= GetWeaponAttack;
        Equipment.instance.onStatsChanged += DeductWeaponAttack;
    }

    public void GetWeaponAttack(Item item)
    {
        if (item != null)
        {
            player.stats.Attack += item.damageModifier;
        }
    }

    public void DeductWeaponAttack(Item item)
    {
        player.stats.Attack = player.stats.baseAttack;
    }

    public void GetBoxCollider(Item item)
    {
        if (item != null)
        {
            for (int i = 0; i < weaponColliders.Length; i++)
            {
                if (weaponColliders[i].name == item.Name)
                {
                    boxCollider.size = weaponColliders[i].size;
                    boxCollider.center = weaponColliders[i].center;
                }
            }
        }
    }

    public void enableWeapon()
    {
        boxCollider.enabled = true;
    }

    public void disableWeapon()
    {
        boxCollider.enabled = false;
    }
 
}
