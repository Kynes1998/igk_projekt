using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PlayerMovement : MonoBehaviour
{
    Animator anim;
    CharacterController controller;
    PlayerController player;

    [SerializeField] private float moveSpeed;
    [SerializeField] private float walkSpeed = 5.0f;
    [SerializeField] private float runSpeed = 7.0f;
    [SerializeField] private float jumpHeight = 0.5f;
    [SerializeField] private float gravity = -9.81f;
    [SerializeField] private float groundCheckDistance = 0.4f;
    [SerializeField] private LayerMask groundLayerMask;
    
    public bool isGrounded;

    private Vector3 moveDirection;
    private Vector3 velocity;

    void Start()
    {
        anim = GetComponent<Animator>();
        controller = GetComponent<CharacterController>();
        player = GetComponent<PlayerController>();
    }

    private void Update()
    {
        Move();
    }
    public void Move()
    {
        isGrounded = Physics.CheckSphere(transform.position, groundCheckDistance, groundLayerMask);

        if(isGrounded && velocity.y < 0)
        {
            velocity.y = -2f;
        }

        float moveX = Input.GetAxis("Horizontal");
        float moveZ = Input.GetAxis("Vertical");

        moveDirection = new Vector3(moveX, 0, moveZ);
        moveDirection = transform.TransformDirection(moveDirection);

        if (isGrounded)
        {
            if (moveDirection != Vector3.zero && !Input.GetKey(KeyCode.LeftShift))
            {
                if (!player.isEquipped)
                {
                    anim.SetFloat("PlayerEquipped", 0.5f);
                    anim.SetFloat("vertical", moveZ);
                    anim.SetFloat("horizontal", moveX);
                }
                else if(player.isEquipped)
                {
                    anim.SetFloat("vertical", moveZ);
                    anim.SetFloat("horizontal", moveX);
                }
                Walk();
            }
            else if (moveDirection != Vector3.zero && Input.GetKey(KeyCode.LeftShift))
            {
                if (!player.isEquipped)
                {
                    anim.SetFloat("PlayerEquipped", 0.5f);
                    anim.SetFloat("vertical", moveZ);
                    anim.SetFloat("horizontal", moveX);
                }
                else if(player.isEquipped)
                {
                    anim.SetFloat("PlayerEquipped", 1f);
                    anim.SetFloat("vertical", moveZ);
                    anim.SetFloat("horizontal", moveX);
                }
                Run();
            }
            else if(moveDirection == Vector3.zero)
            {
                moveSpeed = 0f;
                anim.SetFloat("vertical", moveZ);
                anim.SetFloat("horizontal", moveX);
            }
            if (Input.GetKeyUp(KeyCode.Space))
            {
                Jump();
            }
            moveDirection *= moveSpeed;
        }
        
        controller.Move(moveDirection * Time.deltaTime);
        velocity.y += gravity * Time.deltaTime;

        controller.Move(velocity * Time.deltaTime);
    }


    private void Walk()
    {
        moveSpeed = walkSpeed;
        anim.SetBool("isRunning", false);
    }

    public void Jump()
    {
        anim.SetTrigger("Jump");
        velocity.y = Mathf.Sqrt(jumpHeight * -2 * gravity);
    }

    public void Run()
    {
        moveSpeed = runSpeed;
        anim.SetBool("isRunning", true);
    }

    public void SetPosition(Vector3 pos)
    {
        controller.enabled = false;
        transform.position = pos;
        controller.enabled = true;
    }
    public void SetRotation(float x, float y, float z)
    {
        controller.enabled = false;
        transform.rotation = Quaternion.Euler(x, y, z);
        controller.enabled = true;
    }
}
