using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkillDmgController : MonoBehaviour
{
    public EnemyController enemy;
    PlayerController player;

    private void Start()
    {
        enemy = GameObject.FindGameObjectWithTag("Enemy").GetComponent<EnemyController>();
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
    }

    private void OnParticleCollision(GameObject other)
    {
        if(other.tag == "Enemy")
        {
            enemy = other.GetComponent<EnemyController>();
            if(enemy != null)
            enemy.GetHit(player.stats.MagicAttack);
        }
    }


}
