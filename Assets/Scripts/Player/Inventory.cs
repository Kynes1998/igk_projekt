using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour
{
    public static Inventory instance;

    Equipment equipment;
    public ItemPickUp dropItem;

    PlayerController player;

    public GameObject[] chest;
    ChestController currentChest;

    public bool isInventoryFull;

    private void Awake()
    {
        if(instance != null)
        {
            return;
        }
        instance = this;
    }


    private void Start()
    {
        equipment = Equipment.instance;
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
        chest = GameObject.FindGameObjectsWithTag("Chest");
    }

    public List<Item> items = new List<Item>();

    public delegate void OnInventoryChanged();
    public OnInventoryChanged onInventoryChanged;

    public List<int> GetItemID()
    {
        List<int> temp = new List<int>();

        foreach (Item item in items)
        {
            temp.Add(ItemsDatabase.instance.GetItemsIDs(item));
        }
        return temp;
    }

    public void LoadItems(List<int> ids)
    {
        items.Clear();
        foreach (int id in ids)
        {
            items.Add(ItemsDatabase.instance.GetItemFromID(id));
        }

        if (onInventoryChanged != null)
        {
            onInventoryChanged.Invoke();
        }
    }

    public bool Add(Item item)
    {
        if(items.Count >= 24)
        {
            Debug.Log("No enough space!");
            isInventoryFull = true;
            return false;
        }
        items.Add(item);

        if(onInventoryChanged != null)
        {
            onInventoryChanged.Invoke();
        }

        return true;
    }

    public void Remove(Item item)
    {
        items.Remove(item);

        if (onInventoryChanged != null)
        {
            onInventoryChanged.Invoke();
        }
    }

    public void AddInventoryToEq(Item item)
    {
        for (int i = 0; i < items.Count; i++)
        {
            if (items[i] != null)
            {
                equipment.Add(item);
                Remove(item);
                break;
            }
        }
    }

    public void AddItemToChest(Item item)
    {
        //sprawdzamy czy ktoras skrzynka jest otwarta jezeli tak to przypisujemy j� jako aktualna
        for (int i = 0; i < chest.Length; i++)
        {
            if (chest[i].GetComponent<ChestController>().isOpen)
                currentChest = chest[i].GetComponent<ChestController>();
        }

        //przechodzimy po liscie itemow i jezeli item istnieje to dodajemy go do aktualnej skrzynki
        for (int i = 0; i < items.Count; i++)
        {
            if(items[i] != null && !currentChest.isListFull)
            {
                currentChest.Add(item);
                Remove(item);
                break;
            }
        }
    }

    public void DropItem(Item item)
    {
        if(item != null)
        {
            dropItem.item = item;
            Vector3 pos = new Vector3(player.transform.position.x, 0f, player.transform.position.z + 1.0f);

            Instantiate<ItemPickUp>(dropItem, pos, Quaternion.Euler(0f, 0f, 0f));
            Remove(item);
        }
    }

}
