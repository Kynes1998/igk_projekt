using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stats : MonoBehaviour
{
    public string Name = "Player";

    public int Level = 1;

    public float Experience = 0f;

    public float Health = 100.0f;
    public float MaxHealth = 100.0f;

    public float Mana = 100.0f;
    public float MaxMana = 100.0f;

    public float Stamina = 100.0f;
    public float MaxStamina = 100.0f;

    //atak bazowy
    public float baseAttack = 10.0f;
    public float baseMagicAttack = 10.0f;
    public float baseDefence = 10.0f;

    public float Attack = 10.0f;
    public float MagicAttack = 10.0f;
    public float Defence = 10.0f;

    

    public bool AddExp(float value)
    {
        Experience += value;
        if(Experience >= Level * 200)
        {
            Experience -= Level * 200;
            Level++;
            Health = MaxHealth;
            return true;
        }

        return false;
    }


}
