using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public Animator anim;

    private PlayerMovement movement;

    public CameraController cameraController;

    public Stats stats;
    
    public ItemPickUp itemTarget;

    public bool isPickingUp = false;

    public bool isEquipped = false;

    public bool isEating = false;

    public bool isDrinking = false;

    public bool isAlive = true;

    public bool isUsingSkill = false;

    [SerializeField] float rotateSpeed = 5.0f;

    public GameObject[] skills;

    public GameObject target;

    public GameObject leftHandSlot;

    private EnemyController enemy;

    public delegate void OnSkillsChanged(float magicDmg);
    public event OnSkillsChanged onSkillsChanged;

    private void Awake()
    {
        DontDestroyOnLoad(gameObject);
    }

    void Start()
    {
        anim = GetComponent<Animator>();
        movement = GetComponent<PlayerMovement>();
        enemy = GameObject.FindGameObjectWithTag("Enemy").GetComponent<EnemyController>();

        for (int i = 0; i < skills.Length; i++)
        {
            skills[i].SetActive(false);
        }
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            if (isEating)
            {
                Eat();
            }
            else if (isDrinking)
            {
                Drink();
            }
            else
            {
                Attack();
            }
        }
        if (Input.GetKeyDown(KeyCode.Mouse1))
        {
            StartCoroutine(Block());
        }
        if (Input.GetKeyDown(KeyCode.P))
        {
            StartCoroutine(PickUpItem());
        }
        if(isPickingUp)
        {
            FaceTarget();
        }

        UseSkill();

    }


    private IEnumerator PickUpItem()
    {
       if(itemTarget != null)
       {
           if (itemTarget.CanPickUp)
           {
                movement.enabled = false;
                if(itemTarget.transform.position.y <= 0.8f)
                {
                    isPickingUp = true;
                    anim.SetTrigger("PickUpObject");
                    yield return new WaitForSeconds(3.5f);
                }
                else
                {
                    isPickingUp = true;
                    anim.SetTrigger("PickUpObject2");
                    yield return new WaitForSeconds(3.0f);
                }
                itemTarget.Interact();
                itemTarget = null;
                yield return new WaitForSeconds(0.5f);
                movement.enabled = true;
                isPickingUp = false;
            }
       }
       else
       {
            yield return null;
       }
    }

    private void FaceTarget()
    {
        if (itemTarget != null)
        {
            Vector3 direction = itemTarget.transform.position - transform.position;
            direction.y = 0f;
            Quaternion rotation = Quaternion.LookRotation(direction);
            transform.rotation = Quaternion.Slerp(transform.rotation, rotation, rotateSpeed * Time.deltaTime);
        }
    }

    private void Attack()
    {
        if (!GameController.instance.isInventoryVisible && isAlive)
        {
            if(!isEquipped) //atak bez broni
            {
                anim.SetBool("isEquipped", false);
                anim.SetTrigger("Attack"); 
            }
            else
            {
                anim.SetBool("isEquipped", true);
                anim.SetTrigger("Attack");
            }

        }
    }

    private IEnumerator Block()
    {
        if(!GameController.instance.isInventoryVisible)
        {
            if(isEquipped)
            {
                CurrentShield.instance.boxCollider.enabled = true;
                anim.SetTrigger("ShieldBlock");
                yield return new WaitForSeconds(1.2f);
                CurrentShield.instance.boxCollider.enabled = false;
            }
        }
    }

    private void Eat()
    {
        if (!GameController.instance.isInventoryVisible)
        {
            anim.SetBool("isEating", isEating);
            anim.SetTrigger("Consume");
        }
    }

    private void Drink()
    {
        if (!GameController.instance.isInventoryVisible)
        {
            anim.SetBool("isDrinking", isDrinking);
            anim.SetTrigger("Consume");
        }
    }

    private void UseSkill()
    {
        int number = 0;
        if (Input.GetKeyDown(KeyCode.Alpha1)) number = 1;
        if (Input.GetKeyDown(KeyCode.Alpha2)) number = 2;
        if (Input.GetKeyDown(KeyCode.Alpha3)) number = 3;

        switch(number) 
        {
            case 1: //EarthAttack
                if (stats.Mana >= 30f)
                {
                    stats.Mana -= 30f;
                    anim.SetFloat("SkillType", 0f);
                    anim.SetTrigger("CastSkill");
                    skills[number - 1].SetActive(true);
                    StartCoroutine(WaitForSkill(skills[number - 1], 2.5f));
                }
                break;
            case 2: //FireAttack
                if (stats.Mana >= 50f)
                {
                    stats.Mana -= 50f;
                    anim.SetFloat("SkillType", 0.5f);
                    anim.SetTrigger("CastSkill");
                    skills[number - 1].SetActive(true);
                    StartCoroutine(WaitForSkill(skills[number - 1], 2f));
                }
                break;
            case 3: //WaterAttack
                if (stats.Mana >= 100f)
                {
                    stats.Mana -= 100f;
                    anim.SetFloat("SkillType", 1f);
                    anim.SetTrigger("CastSkill");
                    skills[number - 1].SetActive(true);
                    StartCoroutine(WaitForSkill(skills[number - 1], 2f));
                }
                break;
        }

        if(isUsingSkill || !isAlive)
        {
            movement.enabled = false;
        }
        else
        {
            movement.enabled = true;
        }
    }

    private IEnumerator WaitForSkill(GameObject obj, float time)
    {
        isUsingSkill = true;
        yield return new WaitForSeconds(time);
        obj.SetActive(false);
        isUsingSkill = false;
    }

    public void GetHit(float dmg)
    {
        stats.Health -= (dmg - (stats.Defence / 2));
        if(stats.Health <= 0)
        {
            Die();
        }
    }

    public void Die()
    {
        isAlive = false;
        anim.SetBool("isAlive", isAlive);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "EnemyWeapon" && enemy.isAlive)
        {
            if (Equipment.instance.leftHandSlot.gameObject.activeInHierarchy && CurrentShield.instance.hitBlocked)
            {
                return;
            }
            else if (Equipment.instance.leftHandSlot.gameObject.activeInHierarchy && !CurrentShield.instance.hitBlocked)
            {
                GetHit(enemy.stats.Attack);
            }
            else
            {
                GetHit(enemy.stats.Attack);
            }
        }
        else
        {
            return;
        }
    }

    public void enableWeapon()
    {
        if(Equipment.instance.rightHandSlot.gameObject.activeInHierarchy)
            CurrentWeapon.instance.boxCollider.enabled = true;
    }

    public void disableWeapon()
    {
        if (Equipment.instance.rightHandSlot.gameObject.activeInHierarchy)
            CurrentWeapon.instance.boxCollider.enabled = false;
    }

    public void SetPosition(Vector3 pos)
    {
        movement.SetPosition(pos);
    }

    public void SetRotation(float x, float y, float z)
    {
        movement.SetRotation(x, y, z);
    }
}
