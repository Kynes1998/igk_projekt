using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemPickUp : Interactable
{
    public Item item;
    PlayerController player;
    public bool CanPickUp = false;


    private void Start()
    {
        meshFilter.sharedMesh = item.mesh.sharedMesh;
        meshRenderer.sharedMaterials = item.mesh.sharedMaterials;
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
    }

    public override void Interact()
    {
        base.Interact();
        
        if(CanPickUp && Inventory.instance.Add(item))
        {
           Debug.Log("Picking up: " + item.Name);
           Destroy(gameObject);
        }
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            CanPickUp = true;
            player.itemTarget = this;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            CanPickUp = false;
        }
    }
}
