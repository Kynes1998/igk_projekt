using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interactable : MonoBehaviour
{
    public MeshFilter meshFilter;
    public MeshRenderer meshRenderer;

    public virtual void Interact()
    {
        Debug.Log("Interacted with " + gameObject.name);
    }
}
