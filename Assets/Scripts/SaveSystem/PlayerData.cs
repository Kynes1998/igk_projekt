using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

[System.Serializable]
public class PlayerData 
{
    //Player variables
    public string Name;
    public int Level;
    public float Experience;
    public float Health;
    public float Mana;

    public List<int> inventory;
    public List<int> equipment;

    public int map;
    public float[] position;
    public float[] rotation;

    public PlayerData(PlayerController player)
    {
        Name = player.stats.Name;
        Level = player.stats.Level;
        Experience = player.stats.Experience;
        Health = player.stats.Health;
        Mana = player.stats.Mana;
        inventory = Inventory.instance.GetItemID();
        equipment = Equipment.instance.GetItemID();

        map = SceneManager.GetActiveScene().buildIndex;
        position = new float[3];
        position[0] = player.transform.position.x;
        position[1] = player.transform.position.y;
        position[2] = player.transform.position.z;

        rotation = new float[3];
        rotation[0] = player.transform.eulerAngles.x;
        rotation[1] = player.transform.eulerAngles.y;
        rotation[2] = player.transform.eulerAngles.z;

        Debug.Log("Saved at position: " + " x = " + position[0] + " y = " + position[1] + " z = " + position[2]);
        Debug.Log("Saved at rotation: " + " x = " + rotation[0] + " y = " + rotation[1] + " z = " + rotation[2]);
    }

    public PlayerData(string _name)
    {
        Name = _name;
        Level = 1;
        Experience = 0f;
        Health = 100f;
        Mana = 100f;
        inventory = new List<int>();
        equipment = new List<int>();
        map = 1;
        position = new float[3];
        rotation = new float[3];
    }
}
