using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyMovement : MonoBehaviour
{
    public NavMeshAgent agent;
    Transform player;
    Animator anim;
    PlayerController target;

    public float distanceFromPlayer;
    Vector3 startingPosition;

    public bool isApproachingPlayer = false;
    public bool isTargetInReach;
    public bool isAlive = true;

    public float roamingDistanceX = 3.0f;
    public float roamingDistanceZ = 3.0f;
    public float roamingDelay = 20.0f;
    public float roamingTimer;

    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
        target = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
        anim = GetComponent<Animator>();
        startingPosition = transform.position;
        roamingTimer = Random.Range(0f, roamingDelay);
    }

    void Update()
    {
        if(isAlive && isTargetInReach)
        {
            FollowPlayer();
        }
        if(isAlive)
        {
            anim.SetBool("isWalking", agent.hasPath);
        }
        Roam();
    }

    public void Roam()
    {
        if(isAlive && !isApproachingPlayer)
        {
            roamingTimer -= Time.deltaTime;

            if(roamingTimer <= 0)
            {
                agent.SetDestination(startingPosition + new Vector3(Random.Range(-roamingDistanceX, roamingDistanceX), 0f, Random.Range(-roamingDistanceZ, roamingDistanceZ)));
                roamingTimer = roamingDelay;
            }
        }
    }

    public void Die()
    {
        isAlive = false;
        enabled = false;
        agent.enabled = false;
    }

    public void GoBackToStartingPosition()
    {
        if(isAlive)
        {
            isApproachingPlayer = false;
            agent.SetDestination(startingPosition);
        }
    }

    public void FollowPlayer()
    {
        distanceFromPlayer = Vector3.Distance(transform.position, player.position);

        if(!isApproachingPlayer && target.isAlive)
        {
            isApproachingPlayer = true;
        }

        if(!target.isAlive)
        {
            isTargetInReach = false;
            GoBackToStartingPosition();
        }

        if(player != null && distanceFromPlayer > 1.0f)
        {
            agent.isStopped = false;
            agent.SetDestination(player.position);
        }
        else
        {
            agent.isStopped = true;
            agent.ResetPath();
        }
    }

    public void SetPosition(Vector3 pos)
    {
        agent.Warp(pos);
    }
}
