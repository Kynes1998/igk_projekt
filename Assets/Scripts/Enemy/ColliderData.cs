using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColliderData : MonoBehaviour
{
    public EnemyMovement enemy;

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            enemy.isTargetInReach = true;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            enemy.isTargetInReach = false;
            enemy.GoBackToStartingPosition();
        }
    }
}
