using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyController : MonoBehaviour
{
    private Animator anim;
    private NavMeshAgent agent;
    public EnemyStats stats;
    private PlayerController target;
    public EnemyMovement movement;
    private Rigidbody rb;

    public BoxCollider swordCollider;
    private Collider[] colliders;
    private Rigidbody[] rigidbodies;
    public Collider[] otherColliders;

    public GameObject textName;
    public GameObject healthCanvas;

    public bool isAttacking = false;
    public bool isAlive = true;


    void Start()
    {
        anim = GetComponent<Animator>();
        agent = GetComponent<NavMeshAgent>();
        target = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
        movement = GetComponent<EnemyMovement>();
        rb = GetComponent<Rigidbody>();
        colliders = GetComponentsInChildren<Collider>();
        rigidbodies = GetComponentsInChildren<Rigidbody>();
        disableSword();

        stats.Health = 100;

        foreach (Collider collider in colliders)
        {
            collider.enabled = false;
        }

        foreach (Rigidbody rig in rigidbodies)
        {
            rig.isKinematic = true;
        }

        foreach (Collider collider in otherColliders)
        {
            collider.enabled = true;
        }

        rb.isKinematic = false;
    }

    private void Update()
    {
        if(movement.isApproachingPlayer && isAlive)
        {
            if (Vector3.Distance(transform.position, target.transform.position) < 1.5f && target.isAlive)
            {
                Attack();
            }
            else if((Vector3.Distance(transform.position, target.transform.position) > 1.5f || !target.isAlive))
            {
                StopAttack();
            }
        }

        NameFollowCamera();
    }

    private void Attack()
    {
        isAttacking = true;
        anim.SetBool("isAttacking", isAttacking);
    }
    private void StopAttack()
    {
        isAttacking = false;
        anim.SetBool("isAttacking", isAttacking);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "Weapon")
        {
            Debug.Log("Hit");
            GetHit(target.stats.Attack);
        }
    }

    public void GetHit(float dmg)
    {
        stats.Health -= (dmg - stats.Defence);

        if(stats.Health <= 0)
        {
            Die();
        }
    }

    public void Die()
    {
        target.stats.Experience += stats.Experience;
        movement.Die();
        enabled = false;
        agent.enabled = false;
        isAlive = false;
        healthCanvas.SetActive(false);
        StopAttack();
        RagdollDeath();
        rb.isKinematic = true;
        textName.SetActive(false);
        //StartCoroutine(Decompose());
    }

    /*private IEnumerator Decompose()
    {

        yield return new WaitForSeconds(5.0f);
        
        float timer = 10.0f;

        while(timer > 0)
        {
            timer -= Time.deltaTime;
            
            foreach (Collider collider in colliders)
            {
                collider.enabled = false;
            }
            transform.position += new Vector3(0f, 1 * -Time.deltaTime, 0f);
            yield return null;
        }
        Destroy(gameObject);
    }*/

    public void RagdollDeath()
    {
        anim.enabled = false;
        rb.constraints = RigidbodyConstraints.FreezeAll;
        
        foreach (Collider collider in colliders)
        {
            collider.enabled = true;
        }

        foreach (Rigidbody rig in rigidbodies)
        {
            rig.isKinematic = false;
            rig.useGravity = true;
        }
    }

    public void enableSword()
    {
        swordCollider.enabled = true;
    }

    public void disableSword()
    {
        swordCollider.enabled = false;
    }

    public void NameFollowCamera()
    {
        if (textName != null)
        {
            textName.transform.LookAt(Camera.main.transform.position);
            textName.transform.Rotate(0f, 180, 0f);

            if (movement.isApproachingPlayer)
            {
                textName.GetComponent<TextMesh>().color = Color.red;
            }
            else
            {
                textName.GetComponent<TextMesh>().color = Color.black;
            }
        }
    }

    public EnemyStats GetEnemyStats()
    {
        return stats;
    }

    public void SetPosition(Vector3 pos)
    {
        movement.SetPosition(pos);
    }
}
