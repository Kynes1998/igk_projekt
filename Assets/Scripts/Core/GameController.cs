using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
    public static GameController instance;

    public GameObject playerPrefab;

    public PlayerController player;
    
    public PlayerInfoUI playerInfo;

    private SaveManager saveManager;
    
    public CharacterStats charStats;
    public GameObject charStatsUI;
    private bool isCharStatsVisible = false;

    public InventoryUI inventoryUI;
    public bool isInventoryVisible = false;

    public bool isChestUiVisible = false;

    public GameObject[] chest;
    public ChestUI chestUI;

    public GameObject GUI;

    private void Awake()
    {
        if(instance != null)
        {
            return;
        }
        instance = this;

        DontDestroyOnLoad(gameObject);
    }

    void Start()
    {
        saveManager = SaveManager.instance;
        charStatsUI.SetActive(false);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.B))
        {
            ToggleCharStats();
        }

        if (Input.GetKeyDown(KeyCode.I))
        {
            ToggleInventory();
        }

        if(SceneManager.GetActiveScene().buildIndex == 1)
        InteractWithChest();

        if (SceneManager.GetActiveScene().buildIndex == 0)
        {
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }

        if (isChestUiVisible)
        {
            ChestController.instance.onChestChanged.Invoke();
        }

        if(Input.GetKeyDown(KeyCode.L))
        {
            SaveGame();
        }

    }

    public void RespawnPlayer()
    {
        var obj = GameObject.FindGameObjectWithTag("Player");

        if(obj == null)
        {
            player = Instantiate(playerPrefab).GetComponent<PlayerController>();
        }
        else
        {
            player = obj.GetComponent<PlayerController>();
        }
        playerInfo.RegisterPlayer(player);
        charStats.RegisterPlayer(player);
    }
    public void InteractWithChest()
    {
        for (int i = 0; i < chest.Length; i++)
        {
            if (chest[i].GetComponent<ChestController>().CanOpen && Input.GetKeyDown(KeyCode.E))
            {
                chest[i].GetComponent<ChestController>().isOpen = true;
                chest[i].GetComponent<ChestController>().anim.SetBool("isOpen", true);

                if (chest[i].GetComponent<ChestController>().isOpen)
                {
                    chestUI.Show();
                    isChestUiVisible = true;
                    player.anim.SetTrigger("OpenChest");
                }
            }
            else if (chest[i].GetComponent<ChestController>().isOpen && Input.GetKeyDown(KeyCode.Escape))
            {
                chest[i].GetComponent<ChestController>().isOpen = false;
                chest[i].GetComponent<ChestController>().anim.SetBool("isOpen", false);

                if (!chest[i].GetComponent<ChestController>().isOpen)
                {
                    chestUI.Hide();
                    isChestUiVisible = false;
                }
            }
        }
    }


    private void ToggleCharStats()
    {
        if(!isCharStatsVisible)
        {
            charStatsUI.SetActive(true);
            isCharStatsVisible = true;
        }
        else
        {
            charStatsUI.SetActive(false);
            isCharStatsVisible = false;
        }
    }

    private void ToggleInventory()
    {
        if(!isInventoryVisible)
        {
            inventoryUI.Show();
            isInventoryVisible = true;
        }
        else
        {
            inventoryUI.Hide();
            isInventoryVisible = false;
        }
    }
    public void AddExperience(float value)
    {
        bool hasLeveledUp = player.stats.AddExp(value);

        if(hasLeveledUp)
        {
            Debug.Log("Nowy poziom!");
        }
    }
    public void SaveGame()
    {
        saveManager.SaveGame();
    }
    
    public void ShowGUI()
    {
        GUI.SetActive(true);
    }
    public void HideGUI()
    {
        GUI.SetActive(false);
    }
}
