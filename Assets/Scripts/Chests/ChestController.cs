using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChestController : MonoBehaviour
{
    public static ChestController instance;

    public ChestModel chestModel;

    Inventory inventory;

    public Animator anim;

    public bool CanOpen = false;
    public bool isOpen = false;
    public bool isListFull;

    public delegate void OnChestChanged();
    public OnChestChanged onChestChanged;

    public delegate bool OnSlotsChanged();
    public OnSlotsChanged onSlotsChanged;

    private void Awake()
    {
        if(instance != null)
        {
            return;
        }
        instance = this;
    }

    private void Start()
    {
        inventory = Inventory.instance;
    }

    private void Update()
    {
        if (chestModel.items.Count >= 16)
        {
            isListFull = true;
        }
        else
        {
            isListFull = false;
        }
    }

    public void Add(Item item)
    {
        if (!isListFull)
        {
            chestModel.items.Add(item);

            if (onChestChanged != null)
                onChestChanged.Invoke();
        }
    }

    public void Remove(Item item)
    {
        chestModel.items.Remove(item);

        if (onChestChanged != null)
            onChestChanged.Invoke();
    }

    public void AddItemToInventory(Item item)
    {
        for (int i = 0; i < chestModel.items.Count; i++)
        {
            if(chestModel.items[i] != null && !inventory.isInventoryFull)
            {
                inventory.Add(item);
                Remove(item);
                break;
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            CanOpen = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            anim.SetBool("isOpen", false);
            CanOpen = false;
            isOpen = false;

            if(!isOpen)
            {
                ChestUI.instance.Hide();
                GameController.instance.isChestUiVisible = false;
            }
        }
    }
}
